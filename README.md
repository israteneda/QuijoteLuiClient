# QuijoteLuiClient

Aplicación para enviar los comprobantes electrónicos a los servicios web del SRI Ecuador

### Para compilar y generar el archivo QuijoteLuiClient-1.2.jar
```
$ ant

```
### Para subir al repositorio Maven Local
```
$ mvn install:install-file -Dfile=./dist/QuijoteLuiClient-1.2.jar -DgroupId=com.quijotelui.clientews -DartifactId=QuijoteLuiClient -Dversion=1.2 -Dpackaging=jar
```
#### Documentación
https://www.allku.expert/quijotelui-client/
